#!/bin/bash
echo "Testing Backend Unit and Endpoints"
cd backend
composer test | awk '{print "[Backend Tests]   " $0}'
cd ..
echo "Testing Frontend Unit and E2E"
cd frontend
npm run test-single-run | awk '{print "[FrontendUT Tests]   " $0}'
npm run protractor | awk '{print "[FrontendE2E Tests]   " $0}'

