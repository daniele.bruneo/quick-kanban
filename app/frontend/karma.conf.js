//jshint strict: false
module.exports = function(config) {
  config.set({

    basePath: './app',

    files: [
      'bower_components/angular/angular.js',
      'bower_components/angular-route/angular-route.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'bower_components/ng-sortable/dist/*.js',
      'bower_components/angular-modal-service/dst/*.js',
      'bower_components/jquery/dist/jquery.min.js',
      'bower_components/angular-confirm/dist/*.js',
      'bower_components/angular-xeditable/dist/js/*.js',
      'bower_components/angular-sanitize/angular-sanitize.js',
      'bower_components/angular-color-picker/dist/*.js',
      'bower_components/angular-bootstrap/ui-*.js',
      'bower_components/angularjs-social-login/angularjs-social-login.js',
      'bower_components/angular-cookies/angular-cookies.min.js',
      'app.js',
      'app.config.js',
      'services/*.js',
      'components/**/*.js',
      'modules/**/*.js',
    ],

    autoWatch: true,

    frameworks: ['jasmine'],

    browsers: ['ChromeHeadless'],

    customLaunchers: {
      ChromeHeadless: {
        base: 'Chrome',
        flags: ['--headless', '--disable-gpu', '--remote-debugging-port=9222', 'http://0.0.0.0:9876/']
      }
    },

    plugins: [
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-jasmine',
      'karma-junit-reporter'
    ],

    junitReporter: {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
