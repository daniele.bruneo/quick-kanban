'use strict';

angular.module('myApp.main', ['ngRoute'])

.controller('MainCtrl', function($scope, $rootScope, $cookies, $window, version, $location, socialLoginService, AuthService, BoardService) {
    $rootScope.origin = window.location.origin;
    $rootScope.version = version;
    var user = $cookies.getObject("user");
    if(user){
        $rootScope.user = user;
        $rootScope.logged = true;
        app.log(user);
        BoardService.getUserBoards($rootScope.user.id).then(function(resp){
            $rootScope.userBoards = resp.boards;
            $rootScope.user.boards = resp.boards;
            $rootScope.user.linked_boards = resp.linked_boards;
        });
    } else {
        $rootScope.logged = false;
    }
    app.log("FB loggedin: " + $rootScope.logged);
    
    $rootScope.$on('event:social-sign-in-success', function(event, userDetails){
        app.log("FB login");
        app.log(userDetails);
        AuthService.loginFacebook(userDetails).then(function(resp){
            app.log("Auth")
            app.log(resp);
            if(resp.token){
                app.log("Authorized");
                $rootScope.logged = true;
                $rootScope.user = resp.user;
                $rootScope.token = resp.token;
                $cookies.putObject("user", resp.user);
                $cookies.put("token", resp.token);
                app.log("FB loggedin: " + $rootScope.logged);
                BoardService.getUserBoards($rootScope.user.id).then(function(resp){
                    // $rootScope.userBoards = resp;
                });
            } 
        }).catch(function(resp){
            app.error("Cannot login");
        });

    })
    $rootScope.$on('event:social-sign-out-success', function(event, userDetails){
        app.log("FB logout");
        app.log(userDetails);

        app.log("FB loggedin: " + $rootScope.logged);
    })
    $rootScope.logout = function(){
        $rootScope.user = null;
        $rootScope.logged = false;
        $cookies.remove("user");
        $cookies.remove("token");
        app.log("logging out");
        socialLoginService.logout();
    }
})
.directive('selectAll', function ($window) {
  return {
    restrict: 'A',
    scope: {
      selectAll: '='
    },
    link: function (scope, el) {
      scope.$watch('selectAll', function (val) {
        el.on('click', function () {
            if (!$window.getSelection().toString()) {
                // Required for mobile Safari
                this.setSelectionRange(0, this.value.length)
            }
        });
      });
    }
  };
})
.directive('focusMe', function () {
  return {
    restrict: 'A',
    scope: {
      focusMe: '='
    },
    link: function (scope, el) {
      scope.$watch('focusMe', function (val) {
          setTimeout(function(){
              el[0].focus();
          },700);
      });
    }
  };
});