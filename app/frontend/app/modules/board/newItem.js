angular.module('myApp.board')

.controller('NewItemCtrl', function($scope, $element, column, color, close) {
    $scope.column = column;
    $scope.color = color;
    angular.element('itemName').focus();
    $scope.save = function(){
        app.log('New Item save');
        // Force modal close
        $element.modal('hide');
        jQuery('body').removeClass('modal-open');
        jQuery('.modal-backdrop').remove()
        if (!this.newItemForm.$valid) {
            close(false, 200);  // close, but give 200ms for bootstrap to animate
            return false;
        }
        close(true, 200);
    };
    $scope.close = function(result) {
        app.log('New Item dismiss');
        // Force modal close
        $element.modal('hide');
        jQuery('body').removeClass('modal-open');
        jQuery('.modal-backdrop').remove()
        close(result, 200);
    };
      
});