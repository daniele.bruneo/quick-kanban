'use strict';

angular.module('myApp.board', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/boards/:code-:secret', {
        templateUrl: 'modules/board/board.html',
        controller: 'BoardCtrl'
  });
}])
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/boards/:code', {
        templateUrl: 'modules/board/board.html',
        controller: 'BoardCtrl'
  });
}])
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/new-board', {
        templateUrl: 'modules/board/newBoard.html',
        controller: 'BoardCtrl'
  });
}])
.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/my-boards', {
        templateUrl: 'modules/board/my-boards.html',
        controller: 'BoardCtrl'
  });
}])

.controller('BoardCtrl', function($rootScope, $scope, BoardService, ModalService, $routeParams, $location, $interval, $ngConfirm, editableOptions, editableThemes) {
    var boardCtrl = this;
    
    // Visibility options
    if($rootScope.logged){
        $scope.visibilityOptions = [
            {'value' : 'public', 'label': 'public'},
            {'value' : 'shared', 'label': 'shared'},
            {'value' : 'private', 'label': 'private'},
        ];
        $scope.visibility = "private";
    } else {
        $scope.visibilityOptions = [
            {'value' : 'public', 'label': 'public'},
        ];
        $scope.visibility = "public";
    }

    // Editable options
    editableOptions.theme = 'bs3';
    editableThemes['bs3'].submitTpl = '<button type="submit"><i class="fa fa-check"></i></button>';
    editableThemes['bs3'].cancelTpl = '<button type="submit"><i class="fa fa-times"></i></button>';
    // Background Style
    $scope.bgStyle=0;
    $rootScope.bgStyles = [
        {'value' : 0, 'label': 'default'},
        {'value' : 1, 'label': 'theme 1'},
        {'value' : 2, 'label': 'theme 2'},
        {'value' : 3, 'label': 'theme 3'},
        {'value' : 4, 'label': 'theme 4'},
        {'value' : 5, 'label': 'theme 5'},
        {'value' : 6, 'label': 'theme 6'},
        {'value' : 7, 'label': 'theme 7'},
        {'value' : 8, 'label': 'theme 8'},
        {'value' : 9, 'label': 'theme 9'},
        {'value' : 10, 'label': 'theme 10'},
        {'value' : 11, 'label': 'theme 11'},
        {'value' : 12, 'label': 'theme 12'},
        {'value' : 13, 'label': 'theme 13'},
        {'value' : 14, 'label': 'theme 14'},
        {'value' : 15, 'label': 'theme 15'},
    ];
    
    BoardService.secret = $routeParams.secret;
    $rootScope.secret = BoardService.secret;
    if($routeParams.code){
        loadBoard($routeParams.code);

        // AutoSync Thread
        $scope.syncThread = $interval(function(){
            // avoid resync if: editing fields, modal open, dragging cards, picking color, protected or no code
            if(!$scope.board || jQuery(".editable-input, .modal-backdrop, .as-sortable-drag, .color-picker-open").length 
                || (jQuery("#board-reload").val() == 0
                || $scope.board.properties.includes("protected")
                || !$routeParams.code)){
                app.log('AutoSync sleep...');
                return;
            }
            BoardService.getBoardByCode($routeParams.code).then(function(data) {
                $scope.board = data;
                app.log('AutoSync reload');
            });
        },5000);
    } 


    function loadBoard(code){
        BoardService.getBoardByCode(code).then(function(data) {
            app.log('Board loaded');
            app.log(data);
            $scope.board = data;

            // Watchers for data changes
            $scope.$watch("bgStyle", function(newValue, oldValue) {    
                if(newValue != oldValue){
                    app.log("Background changed: " + newValue);
                    if(newValue==0){
                        newValue="";
                    }
                    jQuery('html').css('background-image','url("img/bg' + newValue + '.jpg")');
                }
            });

            $scope.$watch("board.name", function(newValue, oldValue) {    
                if (oldValue && (newValue != oldValue)){
                    BoardService.patchBoard($scope.board, {'name':newValue});
                }
            });
            
            $scope.board.columns.forEach(function(col, i){
                $scope.$watch("board.columns["+i+"].name", function(newValue, oldValue) {    
                    if (oldValue && (newValue != oldValue)){
                        BoardService.patchColumn($scope.board,$scope.board.columns[i], {'name':newValue});
                    }
                });
                col.items.forEach(function(item, j){
                    $scope.watchItem(j, i);
                });
            });
            
        }).catch(function(resp){
            if(resp.status == 401){
                $rootScope.modal = $ngConfirm({
                    title: "Password required",
                    content: "<input ng-model='password' type='password' onkeyup='event.keyCode == 13 ? angular.element(this).scope().$parent.$root.modal.triggerButton(\"ok\") : null' required focus-me />",
                    type: 'red',
                    buttons: {
                        ok: {
                            text: 'OK',
                            btnClass: 'btn-primary',
                            keys: ['enter','alt'],
                            type: 'button',
                            action: function (scope, button){
                                app.log(scope.password);
                                BoardService.secret = scope.password;
                                $rootScope.secret = scope.password;
                                loadBoard($routeParams.code);
                            }
                        },
                        close: {
                            keys : ['esc'],
                            action: function(){                            
                                $location.path("/");
                            }
                        }
                    }
                });
            } else {
                app.error(resp.error);                
            }
        });
    }

    // Add watchers to given Item
    $scope.watchItem = function(itemIndex, columnIndex){
        var i = itemIndex;
        var c = columnIndex;
        $scope.$watch("board.columns["+c+"].items["+i+"]", function(newItem, oldItem) {
            if ( oldItem && newItem && (newItem != oldItem) && (oldItem.id == newItem.id) && (oldItem.updated_at == newItem.updated_at) ){
                console.log("Old:");
                console.log(oldItem);
                console.log("New:");
                console.log(newItem);
                BoardService.patchItem($scope.board,$scope.board.columns[c].items[i],
                    {
                        'name':newItem.name,
                        'description':newItem.description,
                        'color':newItem.color
                    }
                );
            }
        }, true);
    }

    $scope.addItem = function (column) {
        var colors = ["rgb(238, 238, 135)","rgb(162, 238, 135)","rgb(238, 135, 135)","rgb(135, 145, 238)","rgb(135, 223, 238)","rgb(224, 135, 238)","rgb(238, 183, 135)"];
        var rndcolor =  colors.randomElement();
        
        ModalService.showModal({
            templateUrl: 'modules/board/newItem.html',
            controller: 'NewItemCtrl',
            inputs: {
                column: column,
                color: rndcolor
            }
        }).then(function (modal) {
            app.log('modal then');
            modal.element.modal();
            modal.close.then(function(save) {
                app.log('modal close');
                if(save){
                    BoardService.addItem($scope.board, column, modal.scope)
                        .then(function(resp){
                            var board = resp.board;
                            var column = resp.column;
                            var col_idx = 0;
                            $scope.board = board;
                            app.log('Reloaded board');
                            // identify column index
                            board.columns.forEach(function(el,i){
                                if(el.name == column.name){
                                    col_idx = i
                                }
                            });
                            // add watcher 
                            window.setTimeout(function(){
                                $scope.watchItem(column.items.length, col_idx);                
                            },1000);
                        })
                        .catch(function(response){
                            $ngConfirm({
                                title: "Error saving item",
                                content: response,
                                type: 'red',
                                buttons: {
                                    close: function(){}
                                }
                            });
                        });
                } else {
                    app.log('Cancel');
                }
            });
        });

        
    };


    $scope.$on("$destroy", function(){
        $interval.cancel($scope.syncThread);
    });
    
    $scope.importBoard = function (board) {
        BoardService.importBoard(board,$rootScope.secret)
        .then(function(data){
            app.log('Board imported');
            app.log(data);
            if($rootScope.logged){
                BoardService.getUserBoards($rootScope.user.id).then(function(resp){
                    // $rootScope.userBoards = resp;
                });
            }
            $location.path("/my-boards");
        })
        .catch(function(response){
            app.log("Error importing board");
            $ngConfirm({
                title: "Error",
                content: "Error importing board",
                type: 'red',
                buttons: {
                    close: function(){}
                }
            });
        })           
    };

    $scope.saveNewBoard = function () {
        BoardService.newBoard($scope.name, $scope.code, $scope.visibility, $scope.boardsecret)
        .then(function(data){
            app.log('Board saved');
            app.log(data);
            if($rootScope.logged){
                BoardService.getUserBoards($rootScope.user.id).then(function(resp){
                    // $rootScope.userBoards = resp;
                });
            }
            $location.path("/boards/" + data.code + "-" + $scope.boardsecret);
        })
        .catch(function(response){
            app.log("Error saving board");
            $ngConfirm({
                title: "Error",
                content: "Error creating board",
                type: 'red',
                buttons: {
                    close: function(){}
                }
            });
        })           
    };
    
    $scope.cancelNewBoard = function () {
        app.log('New Board cancelled');
        $location.path('/');
    };

    $scope.deleteBoard = function (board, $event){
        app.log('Deleting board ' + board.name);
        BoardService.deleteBoard(board)
        .then(function(result){
            BoardService.getUserBoards($rootScope.user.id).then(function(resp){
                // $rootScope.userBoards = resp;
            });
            app.log("Deleted successfully")
        })
        .catch(function(response){
            app.log("Error deleting board")
            $ngConfirm({
                title: "Error deleting board",
                content: response,
                type: 'red',
                buttons: {
                    close: function(){}
                }
            });
        })
        $event.preventDefault();
    };
    $scope.deleteLinkedBoard = function (linked_board, $event){
        app.log('Deleting linked board ' + linked_board.board.name);
        BoardService.deleteLinkedBoard(linked_board)
        .then(function(result){
            BoardService.getUserBoards($rootScope.user.id).then(function(resp){
                /*
                $rootScope.userBoards = resp.boards;
                $rootScope.user.boards = resp.boards;
                $rootScope.user.linked_boards = resp.linked_boards;
                */
            });
            app.log("Deleted successfully")
        })
        .catch(function(response){
            app.log("Error deleting linked board")
            $ngConfirm({
                title: "Error deleting linked board",
                content: response,
                type: 'red',
                buttons: {
                    close: function(){}
                }
            });
        })
        $event.preventDefault();
    };
    $scope.deleteItem = function (item){
        app.log('Deleting item ' + item.name);
        BoardService.deleteItem($scope.board,item)
        .then(function(result){
            $scope.board = result.board;
            app.log("Deleted successfully")
        })
        .catch(function(response){
            app.log("Error deleting item")
            $ngConfirm({
                title: "Error deleting item",
                content: response,
                type: 'red',
                buttons: {
                    close: function(){}
                }
            });
        })      
    };

    $scope.moveItem = function($scope,event){
        var board = $scope.board;
        var itemId = event.source.itemScope.item.id;
        var destColumnId = event.dest.sortableScope.$parent.column.id;
        var destPosition =  event.dest.index + 1;
        BoardService.moveItem(board,itemId,destColumnId,destPosition)
            .then(function(result){
                app.log("Moved successfully")
                app.log(result);
            })
            .catch(function(response){
                app.log("Error moving item")
                $ngConfirm({
                    title: "Error",
                    content: "Error moving item",
                    type: 'red',
                    buttons: {
                        close: function(){}
                    }
                });
                moveFailure(event);
            })      
    };

    $scope.moveFailure = function(event) {   
        app.log("Move Failure");
        // revert item back to previous status
        event.dest.sortableScope.removeItem(event.dest.index);
        event.source.itemScope.sortableScope.insertItem(event.source.index, event.source.itemScope.item);
    };
    
    $scope.dragOptions = {
        accept: function (sourceItemHandleScope, destSortableScope) {
            return true;
        },
        dragStart: function (event) {
            jQuery('.as-sortable-drag').css('background-color',event.source.itemScope.item.color);
            return true;
        },
        dragEnd: function (event) {
            return true;
        },
        itemMoved: function (event) {            
            $scope.moveItem($scope,event);
        },
        orderChanged: function(event) {
            $scope.moveItem($scope,event);
        },
        dragMove: function(itemPosition, containment, eventObj) {
            var below = eventObj.clientY > window.innerHeight;
            var above = eventObj.clientY < 55;
            if (below || above) {
                window.scrollBy(0, below ? 20 : -20);
            } 
        },
        containment: '.board .columns'
    };

    $scope.colorpickerEventApi = {
        onChange:  function(api, color, $event) {
            
        },
        onBlur:    function(api, color, $event) {},
        onOpen:    function(api, color, $event) {},
        onClose:   function(api, color, $event) {            
            BoardService.patchItem($scope.board, angular.element(api.getElement()).scope().item, {'color':color});
        },
        onClear:   function(api, color, $event) {},
        onReset:   function(api, color, $event) {},
        onDestroy: function(api, color) {},
    };
    
});

Array.prototype.randomElement = function () {
    return this[Math.floor(Math.random() * this.length)]
}