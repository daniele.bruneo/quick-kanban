'use strict';

describe('myApp.demo-view module', function() {

  beforeEach(module('myApp'));

  describe('DemoView controller', function(){

    it('should be defined', inject(function($controller, DemoService) {
      // var $injector = angular.injector([ 'myApp.demo-view' ]);
      // var DemoService = $injector.get("DemoService");
      //spec body
      var $scope = {};
      var Ctrl = $controller('DemoViewCtrl', {
        '$scope': $scope
      });
      expect(Ctrl).toBeDefined();
    }));

  });
});