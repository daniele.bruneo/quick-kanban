'use strict';

angular.module('myApp.demo-view', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/demo-view', {
    templateUrl: 'modules/demo-view/demo-view.html',
    controller: 'DemoViewCtrl'
  });
}])

.controller('DemoViewCtrl', ['$scope', 'DemoService', function($scope, DemoService) {
  $scope.testText='Lorem ipsum';
  DemoService.getDemo().then(function(demoData) {
    $scope.demoData = demoData;
    console.log(demoData);
  });
}]);