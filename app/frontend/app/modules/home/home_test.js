'use strict';

describe('myApp.home module', function() {

  beforeEach(module('myApp'));

  describe('Home controller', function(){

    it('should be defined', inject(function($controller) {
      var $scope = {};
      //spec body
      var Ctrl = $controller('HomeCtrl', {
        '$scope': $scope
      });
      expect(Ctrl).toBeDefined();
    }));

  });
});