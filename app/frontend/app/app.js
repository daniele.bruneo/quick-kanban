'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
  'ngRoute',
  'as.sortable',
  'angularModalService',
  'ui.bootstrap',
  'cp.ngConfirm',
  'xeditable',
  'color.picker',
  'ngSanitize',
  'socialLogin',
  'ngCookies',
  'myApp.demo-view',
  'myApp.main',
  'myApp.home',
  'myApp.board',
  'myApp.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('');

  $routeProvider.otherwise({redirectTo: '/'});
}]);

// Add a global filter to translate new line to <br>
app.filter('nl2br', function($sce){
  return function(msg,is_xhtml) { 
      var is_xhtml = is_xhtml || true;
      var breakTag = (is_xhtml) ? '<br />' : '<br>';
      var msg = (msg + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
      return $sce.trustAsHtml(msg);
  }
});

app.log = function(message){
  var conf = angular.element(document.documentElement).injector().get('CONFIG');
  if(conf.debug){
    if (typeof message === 'string' || message instanceof String){
      console.log('#QK: ' + message);
    } else {
      console.log('#QK: vvvvvvvvvvvvvvvvvvvvvv');
      console.log(message);
      console.log('#QK: ^^^^^^^^^^^^^^^^^^^^^^');
    }
  }
}

var app_error = null;
app.run(function($http, $cookies, $ngConfirm, $location) {
  app_error = function(error){
        app.log(error);
        $ngConfirm({
          title: "Error",
          content: (error ? error : 'unknown error'),
          type: 'red',
          buttons: {
              close: function(){
                  $location.path("/");
              }
          }
      });
    };
});

app.error = function(error){
  app_error(error);
  
}