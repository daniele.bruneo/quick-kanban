'use strict';

app.service('AuthService', function($http,CONFIG,ModalService,$q,$cookies) {
    var AuthService = this;

    this.loginFacebook = function(userDetails) {
        return $http.post(CONFIG.API_URL + '/auth',
                        userDetails,
                        {
                            headers: {
                                "Content-Type": "application/json"
                            }
                        }
                    )
                .then(function(result) {
                    if(result.data.token){
                        $cookies.put("token",result.data.token);
                        $http.defaults.headers.common['X-Token'] = $cookies.get("token");
                    }
                    return result.data;
                }).catch(function(resp){
                    app.error(resp.data.error);
                });
    };
});

// app.config(['$httpProvider', function($httpProvider) {
//     $httpProvider.defaults.withCredentials = true;
// }]);

app.run(['$http', '$cookies', function($http, $cookies) {
    $http.defaults.headers.common['X-Token'] = $cookies.get("token");
}]);