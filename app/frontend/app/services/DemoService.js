'use strict';

app.factory('DemoService', function($http,CONFIG) {
    return {
         getDemo: function() {
              return $http.get(CONFIG.API_URL + '/demo')
                        .then(function(result) {
                             return result.data;
                         });
         }
    }
 });