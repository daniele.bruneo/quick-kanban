'use strict';

app.service('BoardService', function($http,CONFIG,ModalService,$q,$rootScope,$sanitize) {
    var boardService = this;
    
    this.secret = null;

    this.newBoard = function(name, code, visibility, secret) {
        return $http.post(CONFIG.API_URL + '/boards',
                        { 
                            'name' : name,
                            'code' : code,
                            'visibility' : visibility,
                            'password' : secret
                        },
                        {
                            headers: {
                                "Content-Type": "application/json"
                            }
                        }
                    )
                .then(function(result) {
                    return result.data;
                });
    };

    this.importBoard = function(board, secret) {
        return $http.post(CONFIG.API_URL + '/linked-boards',
                        { 
                            'board_id' : board.id,
                            'secret' : secret,
                        },
                        {
                            headers: {
                                "Content-Type": "application/json"
                            }
                        }
                    )
                .then(function(result) {
                    return result.data;
                });
    };

    this.getUserBoards = function(id) {
        return $http.get(CONFIG.API_URL + '/users/' + id)
                .then(function(result) {
                        $rootScope.userBoards = result.data.boards;
                        $rootScope.user.boards = result.data.boards;
                        $rootScope.user.linked_boards = result.data.linked_boards;
                        return result.data;
                    });
    };

    this.getBoardByCode = function(code) {
        return $http.get(CONFIG.API_URL + '/boards/' + code, { headers: {'X-Secret': this.secret }})
                .then(function(result) {
                        var board = result.data;                        
                        if (board.properties.includes("protected")){
                            board.protected = true;
                        }
                        board.isLinked = false;
                        if($rootScope.user){
                            $rootScope.user.linked_boards.forEach(function(linked,i){
                                if(linked.board.id == board.id){
                                    board.isLinked = true;
                                }
                            });
                        }                        
                        return board;
                    });
    };
    this.moveItem = function(board,itemId,destColumnId,destPosition) {
        // if board is protected mock action        
        if (board.protected && (board.user_id != $rootScope.user.id )){
            return Promise.resolve();
        }
        return $http.put(CONFIG.API_URL + '/boards/' + board.id + '/items/' + itemId + '/position',
                        { 
                            'ColumnId' : destColumnId,
                            'Position' : destPosition
                        },
                        {
                            headers: {
                                "Content-Type": "application/json",
                                'X-Secret': this.secret
                            }
                        }
                    )
                .then(function(result) {
                    return result.data;
                });
    };
    this.deleteBoard = function(board) {
        app.log('deleteBoard');        
        return $http.delete(CONFIG.API_URL + '/boards/' + board.id )
            .then(function(result) {
                console.log('Board deleted succesfully');                
            })
            .catch(function(response){
                app.log("Error deleting board");                
            });
    };
    this.deleteLinkedBoard = function(linked_board) {
        app.log('deleteLinkedBoard');        
        return $http.delete(CONFIG.API_URL + '/linked-boards/' + linked_board.id )
            .then(function(result) {
                console.log('Linked Board deleted succesfully');                
            })
            .catch(function(response){
                app.log("Error deleting board");                
            });
    };
    this.deleteItemPromise = $q.defer();
    this.deleteItem = function(board,item) {
        app.log('deleteItem');
        boardService.deleteItemPromise = $q.defer();
        // if board is protected mock action
        if (board.protected && (board.user_id != $rootScope.user.id )){
            board.columns.forEach(function(col){
                col.items.forEach(function(currentItem,i){
                        if(currentItem == item){
                            col.items.splice(i,1);
                        }
                });
            })
            return Promise.resolve({ 'board': board });
        }
        $http.delete(CONFIG.API_URL + '/boards/' + board.id + '/items/' + item.id, { headers: {'X-Secret': this.secret }})
            .then(function(result) {
                console.log('Item deleted succesfully');
                boardService.getBoardByCode(board.code).then(function(data) {
                    boardService.deleteItemPromise.resolve({ 'board': data });
                });
            })
            .catch(function(response){
                app.log("Error deleting item");
                boardService.deleteItemPromise.reject(response);
            });
        return boardService.deleteItemPromise.promise;
    };
    this.addItemPromise = $q.defer();
    this.addItem = function (board, column, item) {
        app.log('addItem');
        boardService.addItemPromise = $q.defer();
        // if board is protected mock action
        if (board.protected && (board.user_id != $rootScope.user.id )){
            board.columns.forEach(function(col,i){
                if(col.id == item.column.id){
                    col.items.push({
                        'name' : item.name,
                        'description' : item.description,
                        'color' : item.color
                    });
                }
            });
            return Promise.resolve({ 'board': board , 'column' : column});
        }
        $http.post(CONFIG.API_URL + '/boards/' + board.id + '/items',
                    { 
                        'column_id' : item.column.id,
                        'name' : item.name,
                        'description' : item.description,
                        'color' : item.color
                    },
                    {
                        headers: {
                            "Content-Type": "application/json",
                            'X-Secret': this.secret
                        }
                    }
                )
            .then(function(result) {
                app.log('Item saved succesfully');
                boardService.getBoardByCode(board.code).then(function(data) {
                    boardService.addItemPromise.resolve({ 'board': data , 'column' : column});
                });
            })
            .catch(function(response){
                app.log("Error saving new item");
                boardService.addItemPromise.reject(response);
            });
       
        return boardService.addItemPromise.promise;
    };
    this.patchBoard = function(board,fields){
        // if board is protected mock action
        if (board.protected && (board.user_id != $rootScope.user.id )){
            return Promise.resolve();
        }
        return $http.patch(CONFIG.API_URL + '/boards/' + board.id,
                fields,
                {
                    headers: {
                        "Content-Type": "application/json",
                        'X-Secret': this.secret
                    }
                }
            )
        .then(function(result) {
            return result.data;
        });
    };
    this.patchColumn = function(board, column,fields){
        // if board is protected mock action
        if (board.protected && (board.user_id != $rootScope.user.id )){
            return Promise.resolve();
        }
        return $http.patch(CONFIG.API_URL + '/boards/' + column.board_id + '/columns/' + column.id,
                fields,
                {
                    headers: {
                        "Content-Type": "application/json",
                        'X-Secret': this.secret
                    }
                }
            )
        .then(function(result) {
            return result.data;
        });
    };
    this.lastItemPatch = null;
    this.patchItem = function(board,item,fields){
        // if board is protected mock action
        if (board.protected && (board.user_id != $rootScope.user.id )){
            return Promise.resolve();
        }
        var now = new Date();
        var elapsed = now - boardService.lastItemPatch;
        var minElapsedAllowed = 500;
        if(elapsed < minElapsedAllowed){
            app.log('PATCH FLOOD! Last patch was ' + elapsed + 'ms ago');
            return false;
        } else {
            app.log('Patch applied.');
            boardService.lastItemPatch = new Date();
        }
        return $http.patch(CONFIG.API_URL + '/boards/' + board.id + '/items/' + item.id,
                fields,
                {
                    headers: {
                        "Content-Type": "application/json",
                        'X-Secret': this.secret
                    }
                }
            )
            .then(function(result) {
                return result.data;
            })
            .catch(function(resp){
                console.log(resp);
                app.error(resp.data ? resp.data.error : 'unknown');
            });
    };
 });