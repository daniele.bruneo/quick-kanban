app.constant('CONFIG', {
    'API_URL' : 'https://localhost:8080',
    'debug' : true,
    'FB_APP_ID' : '368513496995313',
    'FB_API_VERSION' : 'v2.2',    
  });

  app.config(function(socialProvider, CONFIG){
    // socialProvider.setGoogleKey("YOUR GOOGLE CLIENT ID");
    // socialProvider.setLinkedInKey("YOUR LINKEDIN CLIENT ID");
    socialProvider.setFbKey({appId: CONFIG.FB_APP_ID, apiVersion: CONFIG.FB_API_VERSION});
  });