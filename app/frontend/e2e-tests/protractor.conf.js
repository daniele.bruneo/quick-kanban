//jshint strict: false
exports.config = {

  allScriptsTimeout: 11000,

  specs: [
    '*.js'
  ],

  capabilities: {
    'browserName': 'chrome',
    'acceptInsecureCerts': true,
    'chromeOptions': {
      'args': [
              '--allow-insecure-localhost',
              '--ignore-certificate-errors'
            ]
    }
  },


  baseUrl: 'https://localhost:8000/',

  framework: 'jasmine',

  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  }

};
