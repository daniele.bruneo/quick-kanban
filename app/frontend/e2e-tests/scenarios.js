'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {

  console.log("Redirect to Homepage");
  it('should automatically redirect to / when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/");
  });
  
  
  console.log("DemoView");
  describe('DemoView', function() {
    
    beforeEach(function() {
      browser.get('/#/demo-view');
    });
    
    
    it('should render Demo View when user navigates to /demo-view', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
      toMatch(/partial for demo view/);
    });
    
    it('should contain dynamic test text when user navigates to /demo-view', function() {
      expect(element.all(by.css('[ng-view] quote')).first().getText()).
      toMatch(/Lorem ipsum/);
    });
    
    it('should contain demo data when user navigates to /demo-view', function() {
      expect(element.all(by.css('[ng-view] li')).count()).
      toBe(9);
    });
    
  });
  
  
  console.log("Home");
  describe('Home', function() {
    
    beforeEach(function() {
      browser.get('/');
    });
    
    
    it('should render Home view when user navigates to /', function() {
      expect(element.all(by.css('h1')).first().getText()).
      toMatch(/Quick Kanban/);
    });
    
  });
  
  console.log("Demo Board");
  describe('Demo Board', function() {
    
        beforeEach(function() {
          browser.get('/#/boards/demo');
        });
    
        it('should render Demo view when user navigates to /boards/demo', function() {
          expect(element.all(by.css('h2 span')).first().getText()).
            toMatch(/demo/);
        });
    
      });
});
