<?php

use \Phpmig\Adapter;
use Pimple\Container;
use Illuminate\Database\Capsule\Manager as Capsule;

$container = new Container();

$settings = require __DIR__ . '/src/settings.php';
$container['settings'] = $settings['settings'];

// db
$container['db'] = function ($c) {
	$capsule = new Capsule();
	$capsule->addConnection($c['settings']['db']);
	$capsule->setAsGlobal();
	$capsule->bootEloquent();

	return $capsule;
};

$container['phpmig.adapter'] = function($c) {
	return new Adapter\Illuminate\Database($c['db'], 'migrations');
};
$container['phpmig.migrations_path'] = __DIR__ . DIRECTORY_SEPARATOR . 'migrations';

return $container;
