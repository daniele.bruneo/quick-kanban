<?php

use Phpmig\Adapter;
use Pimple\Container;
use Illuminate\Database\Capsule\Manager as Capsule;

// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// db
$capsule = new Capsule();
$capsule->addConnection($container->get('settings')['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

// 405 Handler
$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        return $c['response']
            ->withStatus(405)
            ->withHeader('Allow', implode(', ', $methods))
            ->withJson(['error' => 'Method not allowed. Method must be one of: ' . implode(', ', $methods)]);
    };
};

// Slim Error Handler
$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
  
      $settings = $c->settings;
  
      $errorCode = 500;
      if (is_numeric($exception->getCode()) && $exception->getCode() > 300  && $exception->getCode() < 600) {
        $errorCode = $exception->getCode();
      }
  
      if ($settings['debug'] == true) {
        $data = [
            'error_code' => $errorCode,
            'error_message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'trace' => explode("\n", $exception->getTraceAsString()),
        ];
      } else {
        $data = [
            'error_code' => $errorCode,
            'error_message' => $exception->getMessage()
        ];
      }
      $c->logger->error($errorCode . " ON " . $exception->getFile() .  ":" . $exception->getLine() . " - " . $exception->getMessage());
  
      return $response->withStatus($errorCode)
                       ->withHeader('Access-Control-Allow-Origin', '*')
                      ->withJson([
                                    'error' => 'Application error',
                                    'error_details' => $data
                                    ]);
    };
  };
 