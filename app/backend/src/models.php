<?php

// Models are loaded from the models folder
$modelFiles = (array) glob( __DIR__ . '/../src/models/*.php' );
foreach($modelFiles as $modelFile) {
	require $modelFile;
}
