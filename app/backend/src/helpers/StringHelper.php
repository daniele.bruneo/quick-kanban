<?php

namespace App\Helpers;

class StringHelper
{
    /* Constructor is private and is uncallable from
     * the outside. This prevents instantiating this class.
    * This is by purpose, because we want a static class.
    */
    private function __construct() {}

    /**
     * Generate random string, from given charset
     * Default charset is: a-zA-Z0-9
     * eg: $hash = rand_string(16);
     * 
     * @param int $length Length of the string to be gerated
     * @param string $chars String containing all the characters that could be used
     * 
     * @return string
     */
    public static function generateRandomString( $length,  $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") {        
        return substr(str_shuffle($chars),0,$length);
    }
}