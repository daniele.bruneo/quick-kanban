<?php

namespace App\Helpers;

class SocialLoginHelper
{
    /* Constructor is private and is uncallable from
     * the outside. This prevents instantiating this class.
    * This is by purpose, because we want a static class.
    */
    private function __construct() {}

    /**
     * Generate random string, from given charset
     * Default charset is: a-zA-Z0-9
     * eg: $hash = rand_string(16);
     * 
     * @param int $length Length of the string to be gerated
     * @param string $chars String containing all the characters that could be used
     * 
     * @return string
     */
    public static function validateToken($provider, $token, $uid) {        
        switch($provider){
            case "facebook":
                $resp = SocialLoginHelper::http("https://graph.facebook.com/me?access_token=$token");
                if($resp["info"]["http_code"] != 200){
                    return false;
                }                
                $respUid = json_decode($resp["body"])->id;
                if($respUid != $uid){
                    return false;
                }
                return true;
            break;
        }
    }

    private static function http($url, $method="GET", $data = "", $contentType = "application/json") {
        $body = json_encode($data);
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);                                                                             
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSLVERSION, 4);
        curl_setopt($ch, CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_USERAGENT,"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: $contentType',
            'Content-Length: ' . strlen($body))
        );  
        $output = curl_exec($ch);
        if($output === false){
            throw new Exception(curl_error($ch));
        }
        $info = curl_getinfo($ch);
        curl_close($ch);
        return array(
          'body' => $output,
          'info' => $info
        );
    }
}