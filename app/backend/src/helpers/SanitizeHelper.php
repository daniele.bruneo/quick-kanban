<?php

namespace App\Helpers;

class SanitizeHelper
{
    /* Constructor is private and is uncallable from
     * the outside. This prevents instantiating this class.
    * This is by purpose, because we want a static class.
    */
    private function __construct() {}

    /**
     * Generate random string, from given charset
     * Default charset is: a-zA-Z0-9
     * eg: $hash = rand_string(16);
     * 
     * @param int $length Length of the string to be gerated
     * @param string $chars String containing all the characters that could be used
     * 
     * @return string
     */
    public static function sanitizeInput($input) {
        $allowedTags = ["<br>","<b>","<i>","<strong>","<em>","<ul>","<ol>","<li>","<a>","<img>"];
        $allowedAttributes = ["src","width","height"];
        $result = strip_tags($input, implode('',$allowedTags));
        // $result = preg_replace('/<(\w+)[^>]*>/', '<$1>', $result);
        $doc = new \DOMDocument(); 
        $doc->loadHTML('<?xml encoding="utf-8" ?>'.$result); 
        foreach($doc->getElementsByTagName('*') as $node) 
        { 
            foreach ($node->attributes as $attr) 
            { 
                if( !in_array($attr->nodeName, $allowedAttributes)){
                    $node->removeAttribute($attr->nodeName);
                }
            }          
        } 
        $wrapper = $doc->getElementsByTagName("p")[0];
        $result = $doc->saveHTML($wrapper);
        $result = strip_tags($result, implode('',$allowedTags));
        $result = preg_replace('/^\n/', '', $result);
        $result = preg_replace('/\n$/', '', $result);
        return $result;
    }
}