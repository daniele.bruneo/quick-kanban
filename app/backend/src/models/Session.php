<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Model;

class Session extends Model {

    protected $table = 'sessions';
    
    public function user() {
        return $this->belongsTo('App\Model\User');
    }

}