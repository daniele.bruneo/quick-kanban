<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Model;

class Item extends Model {

	protected $table = 'items';

	public function column(){
		return $this->belongsTo('App\Model\Column')->with('board');
	}

}