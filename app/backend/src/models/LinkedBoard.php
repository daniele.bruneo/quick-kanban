<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Model;

class LinkedBoard extends Model {

    protected $table = 'linked_boards';
    
    public function board() {
        return $this->belongsTo('App\Model\Board')->with('columns')->with("user");
    }

    public function grantReadAccess($request){
        $token = $request->getHeader("X-Token")[0];
        $session = Session::where("token",$token)->first();
        if(!$token){
            return (object)[
                "granted" => false,
                "denied" => true,
                "error" => (object) [
                    "code" => 400,
                    "message" => 'Private boards need a token'
                ]
            ];
        }            
        if(!$session){
            return (object)[
                "granted" => false,
                "denied" => true,
                "error" => (object) [
                    "code" => 400,
                    "message" => 'Invalid token'
                ]
            ];
        }
        if($session->user->id != $this->user_id){
            return (object)[
                "granted" => false,
                "denied" => true,
                "error" => (object) [
                    "code" => 403,
                    "message" => 'You do not have permissions to access this board'
                ]
            ];
        }            

        return (object)[
            "granted" => false,
            "denied" => false,
            "granted" => true,
            "session" => $session
        ];
    }

    public function grantWriteAccess($request){
        $access = $this->grantReadAccess($request);
        if($access->denied) {
            return $access;
        }        
        return (object)[
            "denied" => false,
            "granted" => true,
            "session" => $access->session
        ];
    }
}