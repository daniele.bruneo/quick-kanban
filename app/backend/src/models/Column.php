<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Model;

class Column extends Model {

	protected $table = 'columns';

    public function board() {
        return $this->belongsTo('App\Model\Board');
    }

    public function items() {
        return $this->hasMany('App\Model\Item')->orderBy('order');
    }
}