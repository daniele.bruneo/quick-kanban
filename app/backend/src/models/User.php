<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Model;

class User extends Model {

    protected $table = 'users';
    
    public function boards() {
        return $this->hasMany('App\Model\Board')->with('columns');
    }

    public function linked_boards() {
        return $this->hasMany('App\Model\LinkedBoard')->with('board');
    }

}