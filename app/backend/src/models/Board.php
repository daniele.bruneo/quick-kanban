<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Model;

use Slim\Http\Request;
use Slim\Http\Response;
use App\Model\Session;

class Board extends Model {

    protected $table = 'boards';
    protected $hidden = array('password');
    
    public function columns() {
        return $this->hasMany('App\Model\Column')->with('items');
    }

    public function user() {
        return $this->belongsTo('App\Model\User');
    }

    public function isProtected() {
        return (strpos($this->properties,"protected") !== false);
    }

    public function grantReadAccess($request){
        $token = $request->getHeader("X-Token") ? $request->getHeader("X-Token")[0] : "";
        $session = Session::where("token",$token)->first();
        if($this->visibility == "private"){
            if(!$token){
                return (object)[
                    "granted" => false,
                    "denied" => true,
                    "error" => (object) [
                        "code" => 400,
                        "message" => 'Private boards need a token'
                    ]
                ];
            }            
            if(!$session){
                return (object)[
                    "granted" => false,
                    "denied" => true,
                    "error" => (object) [
                        "code" => 400,
                        "message" => 'Invalid token'
                    ]
                ];
            }
            if($session->user != $this->user){
                return (object)[
                    "granted" => false,
                    "denied" => true,
                    "error" => (object) [
                        "code" => 403,
                        "message" => 'You do not have permissions to access this board'
                    ]
                ];
            }            
        }
        if($this->visibility == "shared"){
            if($session->user != $this->user){
                $secret = $request->getHeader("X-Secret")[0];
                if(!$secret){
                    return (object)[
                        "granted" => false,
                        "denied" => true,
                        "error" => (object) [
                            "code" => 401,
                            "message" => 'This board require a secret'
                        ]
                    ];
                }
                if(! password_verify($secret,$this->password)){
                    return (object)[
                        "granted" => false,
                        "denied" => true,
                        "error" => (object) [
                            "code" => 401,
                            "message" => 'Secret not valid for this board'
                        ]
                    ];
                }
            }            
        }
        return (object)[
            "granted" => false,
            "denied" => false,
            "granted" => true,
            "session" => $session
        ];
    }

    public function grantWriteAccess($request){
        $access = $this->grantReadAccess($request);
        if($access->denied) {
            return $access;
        }
        if($this->isProtected()){
            if($this->user->id != $access->session->user->id){
                return (object)[
                    "granted" => false,
                    "denied" => true,
                    "error" => (object) [
                        "code" => 403,
                        "message" => 'You do not have permissions to access this board'
                    ]
                ];
            }
        }
        return (object)[
            "denied" => false,
            "granted" => true,
            "session" => $access->session
        ];
    }
}