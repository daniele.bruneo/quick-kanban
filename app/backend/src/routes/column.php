<?php

use Slim\Http\Request;
use Slim\Http\Response;
    
use App\Model\Board;
use App\Model\Column;
use App\Model\Item;
use App\Model\Session;
use App\Model\User;

// PATCH an existing column
$app->patch('/boards/{boardId:\d+}/columns/{id:\d+}', function (Request $request, Response $response, array $args) {
    $boardId = $args['boardId'];
    $id = $args['id'];
    $payload = $request->getParsedBody();
    $column = Column::with("board")->find($id);
    if(!$column){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No column exists with that id']);
    }
    if($column->board_id != $boardId){
        return $response->withStatus(400)
                        ->withJson(['error' => 'That column does not belong to this board']);
    }
    $board = $column->board;
    $access = $board->grantWriteAccess($request);
    if ($access->denied){
        return $response->withStatus($access->error->code)
                        ->withJson(['error' => $access->error->message]);
    }

    $patchableFields = ["name"];
    foreach($payload as $field => $val){
        if(in_array($field, $patchableFields)){
            $column->$field = $val;
        }
    }
    $column->save();
    return $response->withJson($column);
});
