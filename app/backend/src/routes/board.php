<?php

use Slim\Http\Request;
use Slim\Http\Response;
    
use App\Model\Board;
use App\Model\Column;
use App\Model\Item;
use App\Model\Session;
use App\Model\User;


// GET all public boards, including columns and items
// TODO: this route is here for debugging purpose, this should be remove or protected in prod
$app->get('/boards', function (Request $request, Response $response, array $args) {
    $data = Board::with('columns')->where("visibility","public")->get();
    if(!$data){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No board exists']);
    }
    return $response->withJson($data);
});

// GET a board by id
$app->get('/boards/{id:\d+}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $board = Board::with('columns')->find($id);
    if(!$board){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No board exists with that id']);
    }
    $access = $board->grantReadAccess($request);
    if ($access->denied){
        return $response->withStatus($access->error->code)
                        ->withJson(['error' => $access->error->message]);
    }
    return $response->withJson($board);        
});

// GET a board by code
$app->get('/boards/{code:\w+}', function (Request $request, Response $response, array $args) {
    $code = $args['code'];
    $board = Board::with('columns')->where('code',$code)->first();
    if(!$board){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No board exists with that code']);
    }
    $access = $board->grantReadAccess($request);
    if ($access->denied){
        return $response->withStatus($access->error->code)
                        ->withJson(['error' => $access->error->message]);
    }
    return $response->withJson($board);    
});

// POST a new board {name*, code, columns[]}
$app->post('/boards', function (Request $request, Response $response, array $args) {
    $payload = $request->getParsedBody();
    $name = isset($payload["name"]) ? $payload["name"] : "";
    $code = isset($payload["code"]) ? $payload["code"] : "";
    $columns = isset($payload["columns"]) ? $payload["columns"] : "";
    $visibility = isset($payload["visibility"]) ? $payload["visibility"] : "";
    $password = isset($payload["password"]) ? password_hash($payload["password"], PASSWORD_DEFAULT) : null;
    // name is required
    if(!isset($name)){
        return $response->withStatus(400)
                        ->withJson(['error' => 'No name was provided']);
    }
    // code could optionally be provided
    if(!$code) {
        // if code is not provided, generate one
        do{ // iterate until unique
            $code = App\Helpers\StringHelper::generateRandomString(7);
            $count = Board::where('code',$code)->count();
        } while ($count);
    } else {
        // Check code unicity and error if not unique 
        $count = Board::where('code',$code)->count();
        if($count){
            return $response->withStatus(400)
            ->withJson(['error' => 'Code already in use']);
        }
    }
    // columns are optional, if not provided create deafults
    if(!$columns) {
        $columns = ['Backlog .zZ','ToDo ->','Doing ...','Done !!!'];
    }
    // Default visibility is public
    if(!isset($visibility)){
        $visibility = "public";
    }
    $token = $request->getHeader("X-Token");
    if($token){
        $session = Session::where("token",$token)->first();
    }
    if($visibility == "private"){
        if(!$token){
            return $response->withStatus(400)
                            ->withJson(['error' => 'Private boards need a token']);
        }
        if(!$session){
            return $response->withStatus(400)
                            ->withJson(['error' => 'Invalid token']);
        }
    }
    $user_id = null;
    if(isset($session)){
        $user_id = $session->user->id;
    }
    // Create Board
    $board = new Board();
    $board->name = $name;
    $board->code = $code;
    $board->visibility = $visibility;
    $board->password = $password;
    $board->user_id = $user_id;
    $board->save();
    // Create Columns
    foreach($columns as $column){
        $col = new Column();
        $col->board_id = $board->id;
        $col->name = $column;
        $col->save();
    }
    // Reload complete model (Board with Columns)
    $data = Board::with('columns')->find($board->id);
    // If no data are present on db error
    if(!$data){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No board was created']);
    }
    // Everything went ok, return the response
    return $response->withStatus(201)
                    ->withHeader("Location",$this->get('settings')["API_URL"]."/boards/".$data->id)
                    ->withJson($data);
});

// PATCH an existing board
$app->patch('/boards/{id:\d+}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $payload = $request->getParsedBody();    
    $board = Board::find($id);
    if(!$board){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No board exists with that id']);
    }
    
    $access = $board->grantWriteAccess($request);
    if ($access->denied){
        return $response->withStatus($access->error->code)
                        ->withJson(['error' => $access->error->message]);
    }

    $patchableFields = ["name", "visibility"];
    foreach($payload as $field => $val){
        if(in_array($field, $patchableFields)){
            $board->$field = $val;
        }
    }
    if($payload["password"]){
        $board->password =  password_hash($payload["password"], PASSWORD_DEFAULT);
    }
    $board->save();
    return $response->withJson($board);
});

// DELETE an existing board
$app->delete('/boards/{id:\d+}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $board = Board::find($id);
    if(!$board){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No board exists with that id']);
    }

    $access = $board->grantWriteAccess($request);
    if ($access->denied){
        return $response->withStatus($access->error->code)
                        ->withJson(['error' => $access->error->message]);
    }

    $columns = Column::where("board_id",$id)->get();
    foreach($columns as $column){
        Item::where("column_id",$column->id)->delete();
    }
    Board::find($id)->delete();
    $board['status'] = 'success';
    return $response->withJson($board);
});
