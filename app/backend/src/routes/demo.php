<?php

use Slim\Http\Request;
use Slim\Http\Response;
    
use App\Model\Demo;

$app->get('/demo', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/demo' route");
    $data = Demo::all()->toArray();
    return $response->withHeader("content-type","application/json")
                    ->withJson($data);
    
});
