<?php

use Slim\Http\Request;
use Slim\Http\Response;
    
use App\Model\LinkedBoard;
use App\Model\Column;
use App\Model\Item;
use App\Model\Session;
use App\Model\User;


// DELETE an existing board
$app->delete('/linked-boards/{id:\d+}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $linked_board = LinkedBoard::find($id);
    if(!$linked_board){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No linked board exists with that id']);
    }

    $access = $linked_board->grantWriteAccess($request);
    if ($access->denied){
        return $response->withStatus($access->error->code)
                        ->withJson(['error' => $access->error->message]);
    }
    $linked_board->delete();
    $linked_board['status'] = 'success';
    return $response->withJson($linked_board);
});


// POST a new board {name*, code, columns[]}
$app->post('/linked-boards', function (Request $request, Response $response, array $args) {
    $payload = $request->getParsedBody();
    $board_id = $payload["board_id"];
    $secret = $payload["secret"];
    if(!$secret) $secret ="";
    $token = $request->getHeader("X-Token");
    if(!$token){
        return $response->withStatus(400)
                        ->withJson(['error' => 'Private boards need a token']);
    }
    $session = Session::where("token",$token)->first();
    if(!$session){
        return $response->withStatus(400)
                        ->withJson(['error' => 'Invalid token']);
    }
    $user_id = $session->user->id;
    // Create Board
    $board = new LinkedBoard();
    $board->board_id = $board_id;
    $board->secret = $secret;
    $board->user_id = $user_id;
    $board->save();
    $data = LinkedBoard::with('board')->find($board->id);
    // If no data are present on db error
    if(!$data){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No board was created']);
    }
    // Everything went ok, return the response
    return $response->withStatus(201)
                    ->withHeader("Location",$this->get('settings')["API_URL"]."/linked-boards/".$data->id)
                    ->withJson($data);
});

