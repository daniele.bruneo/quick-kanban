<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/basic-test/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/basic-test/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
