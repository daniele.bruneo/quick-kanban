<?php

use Slim\Http\Request;
use Slim\Http\Response;
    
use App\Model\Board;
use App\Model\Column;
use App\Model\Item;
use App\Model\User;
use App\Model\Session;
use App\Helpers\SocialLoginHelper;
use App\Helpers\StringHelper;


// POST a new Authorization request
$app->post('/auth', function (Request $request, Response $response, array $args) {
    $payload = $request->getParsedBody();
    $name = $payload["name"];
    $email = $payload["email"];
    $provider = $payload["provider"];
    $uid = $payload["uid"];
    $imageUrl = $payload["imageUrl"];
    $token = $payload["token"];

    $supportedProviders = ["facebook"];
    if(in_array($provider, $supportedProviders)){
        // Check token validity
        $tokenIsValid = SocialLoginHelper::validateToken($provider, $token, $uid);
        if(!$tokenIsValid){
            return $response->withStatus(400)
                            ->withJson([
                                        'error' => "Token not valid",
                                        'details' => "Provider: $provider Token: $token"
                                        ]); 
        }
        // Check user
        $user = User::where("provider",$provider)->where("uid",$uid)->first();
        if(!$user){
            $data["details"] = "new user";
            $user = new User();
            $user->provider = $provider;
            $user->uid = $uid;
        } else {
            $data["details"] = "existing user";
        }
        $user->name = $name;
        $user->email = $email;
        $user->imageUrl = $imageUrl;
        $user->save();

        $session = Session::where("provider",$provider)->where("provider_token",$token)->first();
        if(!$session){
            $session = new Session();
            $session->user_id = $user->id;
            $session->provider = $provider;
            $session->provider_token = $token;
            $session->token = StringHelper::generateRandomString(16);
            $session->save();
        }        
        $data["user"] = $user;
        $data["token"] = $session->token;
        return $response->withStatus(201)
                        ->withHeader("Location",$this->get('settings')["API_URL"]."/users/$provider/".$uid)
                        ->withJson($data);
    } else {
        return $response->withStatus(400)
                        ->withJson(['error' => "Provider $provider not supported"]);
    }
});