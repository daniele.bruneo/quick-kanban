<?php

use Slim\Http\Request;
use Slim\Http\Response;
    
use App\Model\Board;
use App\Model\Column;
use App\Model\Item;
use App\Model\Session;
use App\Model\User;

// GET a user
$app->get('/users/{id:\d+}', function (Request $request, Response $response, array $args) {
    $id = $args['id'];
    $user = User::with('boards')->with("linked_boards")->find($id);
    if(!$user){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No user exist with that id']);
    }
    $token = $request->getHeader("X-Token");
    if(!$token){
        return $response->withStatus(400)
                        ->withJson(['error' => 'User need a token']);
    }
    $session = Session::where("token",$token)->first();
    if(!$session){
        return $response->withStatus(400)
                        ->withJson(['error' => 'Invalid token']);
    }
    if($session->user->id != $user->id){
        return $response->withStatus(403)
                        ->withJson(['error' => 'You do not have permissions to access this user']);
    }
    return $response->withJson($user);

});