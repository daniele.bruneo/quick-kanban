<?php

use Slim\Http\Request;
use Slim\Http\Response;
    
use App\Model\Board;
use App\Model\Column;
use App\Model\Item;
use App\Model\Session;
use App\Model\User;

use App\Helpers\SanitizeHelper;

// GET an existing item
$app->get('/boards/{boardId:\d+}/items/{id:\d+}', function (Request $request, Response $response, array $args) {
    $boardId = $args['boardId'];
    $id = $args['id'];
    $data = Item::with('column')->find($id);
    if(!$data){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No item exists with that id']);
    }
    $board = $data->column->board;
    $access = $board->grantReadAccess($request);
    if ($access->denied){
        return $response->withStatus($access->error->code)
                        ->withJson(['error' => $access->error->message]);
    }
    return $response->withJson($data);
});

// PATCH an existing item
$app->patch('/boards/{boardId:\d+}/items/{id:\d+}', function (Request $request, Response $response, array $args) {
    $boardId = $args['boardId'];
    $id = $args['id'];
    $payload = $request->getParsedBody();
    $data = Item::with('column')->find($id);
    if(!$data){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No item exists with that id']);
    }    
    $board = $data->column->board;
    $access = $board->grantWriteAccess($request);
    if ($access->denied){
        return $response->withStatus($access->error->code)
                        ->withJson(['error' => $access->error->message]);
    }
   
    $patchableFields = ["name","description","color"];
    foreach($payload as $field => $val){
        if(in_array($field, $patchableFields)){
            $data->$field = SanitizeHelper::sanitizeInput($val);
        }
    }
    $data->save();
    return $response->withJson($data);
});

// POST a new item
$app->post('/boards/{boardId:\d+}/items', function (Request $request, Response $response, array $args) {
    $boardId = isset($args['boardId']) ? intval($args['boardId']) : "";
    $itemId = isset($args['itemId']) ? intval($args['itemId']) : "";
    $payload = $request->getParsedBody();
    $columnId = isset($payload['column_id']) ? $payload['column_id'] : "";
    $name = isset($payload['name']) ? $payload['name'] : "";
    $description = isset($payload['description']) ? SanitizeHelper::sanitizeInput($payload['description']) : "";
    $color = isset($payload['color']) ? $payload['color'] : "";
    // Validations and data retrieving
    if(!$columnId){
        return $response->withStatus(400)
        ->withJson(['error' => 'No column id was provided']);
    }
    if(!$name){
        return $response->withStatus(400)
        ->withJson(['error' => 'No name was provided']);
    }
    if(!$color) $color = "#E88";
    $board = Board::find($boardId);
    if(!$board){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No board exists with that id']);
    }
    $access = $board->grantWriteAccess($request);
    if ($access->denied){
        return $response->withStatus($access->error->code)
                        ->withJson(['error' => $access->error->message]);
    }

    $column = Column::find($columnId);
    if($column->board_id != $board->id){
        return $response->withStatus(400)
                        ->withJson(['error' => 'Destination column does not belong to this board']);
    }    
   
    // Auto assign order
    $order =  Item::where('column_id', $columnId)->max('order') + 1;
    // Item creation
    $item = new Item();
    $item->column_id = $columnId;
    $item->order = $order;
    $item->name = $name;
    $item->color = $color;
    $item->description = is_null($description) ? '' : $description;
    $item->save();
    return $response->withStatus(201)
                    ->withHeader("Location",$this->get('settings')["API_URL"]."/boards/items/".$item->id)
                    ->withJson($item);
});

// PUT a new position to an item (move an item to another position)
$app->put('/boards/{boardId:\d+}/items/{itemId:\d+}/position', function (Request $request, Response $response, array $args) {
    $boardId = intval($args['boardId']);
    $itemId = intval($args['itemId']);
    $payload = $request->getParsedBody();
    $destColumnId = $payload['ColumnId'];
    $destPosition = $payload['Position'];
    // Validations and data retrieving
    if(!isset($destColumnId)){
        return $response->withStatus(400)
                        ->withJson(['error' => 'No destination column id was provided']);
    }
    if(!isset($destPosition)){
        return $response->withStatus(400)
                        ->withJson(['error' => 'No destination position was provided']);
    }

    $board = Board::find($boardId);
    if(!$board){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No board exists with that id']);
    }
    $access = $board->grantWriteAccess($request);
    if ($access->denied){
        return $response->withStatus($access->error->code)
                        ->withJson(['error' => $access->error->message]);
    }

    $item = Item::with('column')->find($itemId);
    if (!$item) {
        return $response->withStatus(400)
                        ->withJson(['error' => 'No Item exists with that id']);
    }
    if($item->column->board->id != $boardId){
        return $response->withStatus(400)
                        ->withJson(['error' => 'No Item with that id belongs to this board']);
    }

    $column = Column::find($destColumnId);
    if($column->board_id != $board->id){
        return $response->withStatus(400)
                        ->withJson(['error' => 'Destination column does not belong to this board']);
    }
    
    if(!$order || $order<0) $order = 1;
    
    // Moving logic
    if($destColumnId == $item->column->id){
        // Item is moving within the same column
        if($destPosition == $item->order){
            // Nothing to move
            $data['details'] = 'same-position';
        } else if ($destPosition > $item->order) {
            // Moving down
            Item::where([
                ['column_id', '=', $destColumnId],
                ['order', '>=', $item->order],
                ['order', '<=', $destPosition],
            ])->decrement('order');
            $data['details'] = 'same-column-down';
        } else {
            // Moving up
            Item::where([
                ['column_id', '=', $destColumnId],
                ['order', '>=', $destPosition],
                ['order', '<=', $item->order],
            ])->increment('order');
            $data['details'] = 'same-column-up';
        }
    } else {
        // Item is moving across different columns
        // Reordering items of old column
        Item::where([
            ['column_id', '=', $item->column->id],
            ['order', '>', $item->order],
            ])->decrement('order');
        // Reordering items of new column
        Item::where([
            ['column_id', '=', $destColumnId],
            ['order', '>=', $destPosition],
        ])->increment('order');
        $item->column_id = $destColumnId;
        $data['details'] = 'different-column';
    }
    $item->order = $destPosition;
    $item->update();
    $data['BoardId'] = $boardId;
    $data['ItemId'] = $itemId;
    $data['ColumnId'] = $destColumnId;
    $data['Position'] = $destPosition;
    return $response->withJson($data);
});

// DELETE an item
$app->delete('/boards/{boardId:\d+}/items/{itemId:\d+}', function (Request $request, Response $response, array $args) {
    $boardId = intval($args['boardId']);
    $itemId = intval($args['itemId']);
    // Validations and data retrieving
    $board = Board::find($boardId);
    if(!$board){
        return $response->withStatus(404)
                        ->withJson(['error' => 'No board exists with that id']);
    }
    $access = $board->grantWriteAccess($request);
    if ($access->denied){
        return $response->withStatus($access->error->code)
                        ->withJson(['error' => $access->error->message]);
    }
    $item = Item::with('column')->find($itemId);
    if (!$item) {
        return $response->withStatus(400)
                        ->withJson(['error' => 'No Item exists with that id']);
    }
    if($item->column->board->id != $boardId){
        return $response->withStatus(400)
                        ->withJson(['error' => 'No Item with that id belongs to this board']);
    }
    
    // Delete logic
    Item::where([
        ['column_id', '=', $item->column->id],
        ['order', '>', $item->order],
    ])->decrement('order');
    $item->delete();
    $data['status'] = 'success';
    return $response->withJson($data);
});