<?php

// Helpers are loaded from the helpers folder
$modelFiles = (array) glob( __DIR__ . '/../src/helpers/*.php' );
foreach($modelFiles as $modelFile) {
	require $modelFile;
}
