<?php

// Routes are loaded from the routes folder
$routeFiles = (array) glob( __DIR__ . '/../src/routes/*.php' );
foreach($routeFiles as $routeFile) {
	require $routeFile;
}
