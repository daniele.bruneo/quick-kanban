<?php

use Phpmig\Migration\Migration;

class BoardAddProperties extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();   

        $container['db']::schema()->table('boards', function ($table) {
            $table->string('properties')->after('name')->default('');
        });   
        $container['db']::table('boards')
        ->where('id', '<=',2)
        ->update(['properties' => 'protected']);

    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();   

        $container['db']::schema()->table('boards', function ($table) {
            $table->dropColumn('properties');
        });   

    }
}
