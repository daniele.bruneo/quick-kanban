<?php

use Phpmig\Migration\Migration;

class LinkedBoards extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();   
        // Linked Boards
        $container['db']::schema()->create('linked_boards', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('board_id')->unsigned();
            $table->string('secret');
            $table->timestamps();
        });
        $container['db']::schema()->table('linked_boards', function ($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('board_id')->references('id')->on('boards')->onDelete('cascade');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer(); 
        
        $container['db']::schema()->drop('linked_boards');
    }
}
