<?php

use Phpmig\Migration\Migration;

class QuickKanbanSampleData extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();   
        // Board
        $container['db']::table('boards')->insert([
            'id' => 1,
            'code' => 'demo',
            'name' => 'Demo Board'
        ]);
        // Columns
        $container['db']::table('columns')->insert([
            [
            'id' => 1,
            'board_id' => 1,
            'name' => 'Backlog',
            ],
            [
            'id' => 2,
            'board_id' => 1,
            'name' => 'ToDo',
            ],
            [
            'id' => 3,
            'board_id' => 1,
            'name' => 'Doing',
            ],
            [
            'id' => 4,
            'board_id' => 1,
            'name' => 'Done',
            ]
        ]);
        // Items
        $container['db']::table('items')->insert([
            [
            'id' => 1,
            'column_id' => 1,
            'order' => 0,
            'name' => 'item in Backlog',
            'description' => 'This is an item sitting in Backlog',
            ],
            [
            'id' => 2,
            'column_id' => 1,
            'order' => 1,
            'name' => 'another item in Backlog',
            'description' => 'This is another item sitting in Backlog',
            ],
            [
            'id' => 3,
            'column_id' => 2,
            'order' => 0,
            'name' => 'item selected as Todo',
            'description' => 'This is an item waiting do be done',
            ],
            [
            'id' => 4,
            'column_id' => 3,
            'order' => 0,
            'name' => 'item being done',
            'description' => 'This item is being done right now',
            ],
            [
            'id' => 5,
            'column_id' => 4,
            'order' => 0,
            'name' => 'item completed',
            'description' => 'This item was done    ',
            ],
            
        ]);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer(); 

        $container['db']::table('items')->delete();
        $container['db']::table('columns')->delete();
        $container['db']::table('boards')->delete();
    }
}
