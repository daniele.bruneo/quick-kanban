<?php

use Phpmig\Migration\Migration;

class UserTable extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();   
        // Users
        $container['db']::schema()->create('users', function ($table) {
            $table->increments('id');
            $table->string('provider');
            $table->string('uid');
            $table->string('name');
            $table->string('email');
            $table->string('imageUrl')->nullable()->default(null);
            $table->timestamps();
        });
        $container['db']::schema()->table('users', function ($table) {
            $table->unique(['provider', 'uid']);
        });
        // Sessions
        $container['db']::schema()->create('sessions', function ($table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('provider');
            $table->string('provider_token');
            $table->string('token')->unique();
            $table->timestamps();
        });
        $container['db']::schema()->table('sessions', function ($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unique(['provider', 'provider_token']);
        });
        // Board
        $container['db']::schema()->table('boards', function ($table) {
            $table->integer("user_id")->nullable()->unsigned()->default(null)->after("properties");
            $table->string("visibility")->default("public")->after("user_id");
            $table->string("password")->nullable()->default(null)->after("visibility");
        });
        $container['db']::schema()->table('boards', function ($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->index("visibility");
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer(); 
        
        $container['db']::schema()->drop('sessions');
        $container['db']::schema()->table('boards', function ($table) {
            $table->dropForeign('boards_user_id_foreign');
            $table->dropColumn('user_id');
            $table->dropColumn('visibility');
            $table->dropColumn('password');
        });   
        $container['db']::schema()->drop('users');
    }
}
