<?php

use Phpmig\Migration\Migration;

class QuickKanbanBasicTables extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();   
        // Board
        $container['db']::schema()->create('boards', function ($table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('name');
            $table->timestamps();
        });
        // Columns
        $container['db']::schema()->create('columns', function ($table) {
            $table->increments('id');
            $table->integer('board_id')->unsigned();
            $table->string('name');
            $table->timestamps();
        });
        $container['db']::schema()->table('columns', function ($table) {
            $table->foreign('board_id')->references('id')->on('boards')->onDelete('cascade');
        });
        // Items
        $container['db']::schema()->create('items', function ($table) {
            $table->increments('id');
            $table->integer('column_id')->unsigned();
            $table->tinyInteger('order');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
        });
        $container['db']::schema()->table('items', function ($table) {
            $table->foreign('column_id')->references('id')->on('columns')->onDelete('cascade');
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer(); 

        $container['db']::schema()->drop('items');
        $container['db']::schema()->drop('columns');
        $container['db']::schema()->drop('boards');
    }
}
