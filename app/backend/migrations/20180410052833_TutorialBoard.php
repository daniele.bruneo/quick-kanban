<?php

use Phpmig\Migration\Migration;

class TutorialBoard extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();   
        // Board
        $container['db']::table('boards')->insert([
            'id' => 3,
            'code' => 'tutorial',
            'properties' => 'protected',
            'name' => 'Tutorial - Have a look at the cards'
        ]);
        // Columns
        $container['db']::table('columns')->insert([
            [
            'id' => 9,
            'board_id' => 3,
            'name' => 'Backlog',
            ],
            [
            'id' => 10,
            'board_id' => 3,
            'name' => 'ToDo',
            ],
            [
            'id' => 11,
            'board_id' => 3,
            'name' => 'Doing',
            ],
            [
            'id' => 12,
            'board_id' => 3,
            'name' => 'Done',
            ]
        ]);
        // Items
        $container['db']::table('items')->insert([
            [
            'column_id' => 9,
            'order' => 1,
            'name' => 'Backlog column',
            'description' => 'Here is where it all begins. One by one, each card will make its way through columns to the "Done" stage.',
            'color' => 'rgb(238, 136, 136)'
            ],
            [
            'column_id' => 9,
            'order' => 2,
            'name' => 'You can add Items',
            'description' => 'Just click on the plus sign',
            'color' => '#EE8'
            ],
            [
            'column_id' => 9,
            'order' => 3,
            'name' => 'Boards are public',
            'description' => 'Every board is assigned an URL you can share. Everyone can update it and see the board updating in real time with other changes. Go create a new board and try it yourself',
            'color' => 'rgb(189, 136, 238)'
            ],
            [
            'column_id' => 10,
            'order' => 1,
            'name' => 'Todo column',
            'description' => 'Here you put what you selected, from the backlog, to do in the near future.',
            'color' => 'rgb(238, 221, 136)'
            ],
            [
            'column_id' => 10,
            'order' => 2,
            'name' => 'Card color can be changed',
            'description' => 'Just go over the card and click the bottom right corner to reveal color picker.',
            'color' => 'rgb(117, 207, 117)'
            ],
            [
            'column_id' => 10,
            'order' => 3,
            'name' => 'You can create new boards',
            'description' => 'Go create a new board and try it yourself.',
            'color' => 'rgb(238, 238, 136)'
            ],
            [
            'column_id' => 11,
            'order' => 1,
            'name' => 'Doing column',
            'description' => 'This column should contain what you are doing right now.',
            'color' => 'rgb(136, 190, 238)'
            ],
            [
            'column_id' => 11,
            'order' => 2,
            'name' => 'Cards can move around',
            'description' => 'Just drag them. To add a card to an empty column, put it at the very top.',
            'color' => '#EE8'
            ],
            [
            'column_id' => 11,
            'order' => 3,
            'name' => 'Column names are editable',
            'description' => 'just click on them and rename them as you want',
            'color' => 'rgb(233, 233, 211'
            ],
            [
            'column_id' => 12,
            'order' => 1,
            'name' => 'Done Column',
            'description' => 'Where everything should end.',
            'color' => 'rgb(173, 238, 136)'
            ],
            [
            'column_id' => 12,
            'order' => 2,
            'name' => 'Cards can be edited and deleted',
            'description' => 'Just click on the card to edit title and details. Click on the top right X to delete a card (it cannot be reverted)',
            'color' => 'rgb(245, 174, 119)'
            ],
            [
            'column_id' => 12,
            'order' => 3,
            'name' => 'Choose background',
            'description' => 'Background can be changed. Look at the bottom of the page. (this is not permanent)',
            'color' => 'rgb(139, 139, 212)'
            ],
            
            
        ]);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();   
        
        $container['db']::table('items')->where('column_id','>',8)->delete();
        $container['db']::table('columns')->where('board_id','=',3)->delete();
        $container['db']::table('boards')->where('id','=',3)->delete();
    }
}
