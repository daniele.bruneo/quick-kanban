<?php

use Phpmig\Migration\Migration;

class QkBoardData extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();   
        // Board
        $container['db']::table('boards')->insert([
            'id' => 2,
            'code' => 'qk',
            'name' => 'Quick Kanban Project status'
        ]);
        // Columns
        $container['db']::table('columns')->insert([
            [
            'id' => 5,
            'board_id' => 2,
            'name' => 'Backlog',
            ],
            [
            'id' => 6,
            'board_id' => 2,
            'name' => 'ToDo',
            ],
            [
            'id' => 7,
            'board_id' => 2,
            'name' => 'Doing',
            ],
            [
            'id' => 8,
            'board_id' => 2,
            'name' => 'Done',
            ]
        ]);
        // Items
        $container['db']::table('items')->insert([
            [
            'column_id' => 5,
            'order' => 0,
            'name' => 'Customizable Card color',
            'description' => 'ability to chose the color of a card',
            ],
            [
            'column_id' => 5,
            'order' => 1,
            'name' => 'Customizable Columns',
            'description' => 'Add the ability to add/remove and rename columns',
            ],
            [
            'column_id' => 5,
            'order' => 2,
            'name' => 'Column WIP limit',
            'description' => 'WIP limits are now managed statically. Allow the user to choose a WIP limit for each column',
            ],
            [
            'column_id' => 5,
            'order' => 3,
            'name' => 'Editable Card',
            'description' => 'Card should be editable',
            ],
            [
            'column_id' => 5,
            'order' => 4,
            'name' => 'Tags',
            'description' => 'Item should be able to contains tag These should be visually presented as little lables attached to the card Tags should have a label and a custom color',
            ],
            [
            'column_id' => 5,
            'order' => 5,
            'name' => 'Password protected boards',
            'description' => '',
            ],
            [
            'column_id' => 6,
            'order' => 0,
            'name' => 'Check Board code uniqueness',
            'description' => 'Add control on code uniqueness during board creation in /boards POST route',
            ],
            [
            'column_id' => 6,
            'order' => 1,
            'name' => 'Refactor AddItem UI logic',
            'description' => 'Modal logic of the AddItem workflow should be properly splitted between Service and Controller',
            ],
            [
            'column_id' => 7,
            'order' => 0,
            'name' => 'My best',
            'description' => '',
            ],
            [
            'column_id' => 8,
            'order' => 0,
            'name' => 'Sample data for QK Board',
            'description' => 'Add sample data for /boards/qk',
            ],
            [
            'column_id' => 8,
            'order' => 1,
            'name' => 'Board Creation',
            'description' => 'User can create a board and assign a name and optionally a code. If code is not assigned a random one is generated.',
            ],
            [
            'column_id' => 8,
            'order' => 2,
            'name' => 'Item creation',
            'description' => '',
            ],
            [
            'column_id' => 8,
            'order' => 3,
            'name' => 'Item drag&drop across columns',
            'description' => '',
            ],
            [
            'column_id' => 8,
            'order' => 4,
            'name' => 'Item delete',
            'description' => '',
            ],
            
            
        ]);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();   
        
        $container['db']::table('items')->where('column_id','>',4)->delete();
        $container['db']::table('columns')->where('board_id','=',2)->delete();
        $container['db']::table('boards')->where('id','=',2)->delete();
    }
}
