<?php

use Phpmig\Migration\Migration;

class InitialTestSchema extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();   
        $container['db']::schema()->create('demo', function ($table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
        for ($i=0; $i<9; $i++){
            $container['db']::table('demo')->insert([
                'name' => str_random(10)
            ]);
        }        
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer(); 
        $container['db']::schema()->drop('demo');
    }
}
