<?php

use Phpmig\Migration\Migration;

class AddColorField extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();   

        $container['db']::schema()->table('items', function ($table) {
            $table->string('color')->after('order')->default('#EE8');
        });   
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();   

        $container['db']::schema()->table('items', function ($table) {
            $table->dropColumn('color');
        });   

    }
}
