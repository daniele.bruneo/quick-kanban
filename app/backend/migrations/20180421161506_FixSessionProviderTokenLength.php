<?php

use Phpmig\Migration\Migration;

class FixSessionProviderTokenLength extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();   
        // Sessions
        $container['db']::schema()->table('sessions', function ($table) {
            $table->dropUnique('sessions_provider_provider_token_unique');
        });
        $container['db']::schema()->table('sessions', function ($table) {
            $table->string('provider_token',350)->change();
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer(); 
        // Sessions
        $container['db']::schema()->table('sessions', function ($table) {
            $table->unique(['provider', 'provider_token']);
            $table->string('provider_token')->change();
        });        

    }
}
