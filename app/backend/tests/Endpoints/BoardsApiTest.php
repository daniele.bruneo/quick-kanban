<?php

class BoardsApiTest extends BaseApiTest
{

    public function testGetBoardsContainsDemo() {
        $this->printToConsole(__METHOD__);
        
        $url = $this->api_url."/boards";
        $response = $this->get($url)["body"];
        
        $expected = '"code":"demo"';
        
        $this->assertContains($expected,$response);
    }

    public function testPostBoard() {
        $this->printToConsole(__METHOD__);

        $url = $this->api_url."/boards";
        $payload = ["name" => "ApiTestBoard"];
        $response = $this->post($url, $payload);
        $data = json_decode($response["body"]);
        
        $this->setVal("id", $data->id);
        $this->setVal("code", $data->code);
        $this->setVal("first_col_id", $data->columns[0]->id);

        $this->printToConsole("Got id: ".$data->id);
        $this->printToConsole("Got code: ".$data->code);

        $this->printToConsole("Got First col id: ".$data->columns[0]->id);

        $expected = 'ApiTestBoard';
        $this->assertContains($expected,$data->name);
    }

    public function testGetBoardById() {
        $this->printToConsole(__METHOD__);

        $id = $this->getVal("id");
        $this->printToConsole("Found id: ".$id);
        
        $url = $this->api_url."/boards/".$id;
        $payload = ["name" => "ApiTestBoard"];
        $response = $this->get($url);
        $data = json_decode($response["body"]);
        
        $expected = 'ApiTestBoard';
        $this->assertContains($expected,$data->name);
    }

    public function testGetBoardByCode() {
        $this->printToConsole(__METHOD__);

        $code = $this->getVal("code");
        $this->printToConsole("Found code: ".$code);
        
        $url = $this->api_url."/boards/".$code;
        $payload = ["name" => "ApiTestBoard"];
        $response = $this->get($url);
        $data = json_decode($response["body"]);
        
        $expected = 'ApiTestBoard';
        $this->assertContains($expected,$data->name);
    }

    public function testPostItem() {
        $this->printToConsole(__METHOD__);

        $boardId = $this->getVal("id");
        $colId = $this->getVal("first_col_id");

        $url = $this->api_url."/boards/".$boardId."/items";
        $payload = ["name" => "ApiTestItem", "board_id" => $boardId, "column_id" => $colId];
        $response = $this->post($url, $payload);
        $data = json_decode($response["body"]);
      
        $this->setVal("item_id", $data->id);

        $this->printToConsole("Got item_id: ".$data->id);
        
        $expected = 'ApiTestItem';
        $this->assertContains($expected,$data->name);
    }

    public function testPatchItem() {
        $this->printToConsole(__METHOD__);

        $boardId = $this->getVal("id");
        $id = $this->getVal("item_id");
        $this->printToConsole("Found item_id: ".$id);

        $url = $this->api_url."/boards/".$boardId."/items/".$id;
        $payload = ["name" => "ApiTestItemEdited"];
        $response = $this->patch($url, $payload);
        $data = json_decode($response["body"]);
        
        $expected = 'ApiTestItemEdited';
        $this->assertContains($expected,$data->name);
    }

    public function testDeleteBoard() {
        $this->printToConsole(__METHOD__);

        $id = $this->getVal("id");
        $this->printToConsole("Deleting id: ".$id);
        
        $url = $this->api_url."/boards/".$id;
        $payload = ["name" => "ApiTestBoard"];
        $response = $this->delete($url);
        $data = json_decode($response["body"]);

        $expected = 'success';
        $this->assertContains($expected,$data->status);
    }

}