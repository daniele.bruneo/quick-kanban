<?php

// Base class for API endpoints tests
class BaseApiTest extends PHPUnit_Framework_TestCase
{
    protected $api_url = "https://localhost:8080";

    protected function &scope(){
        static $storage = [];
        return $storage;
    } 

    protected function setVal($key, $value){
        $this->scope()[$key] = $value;
    }

    protected function getVal($key){
        return $this->scope()[$key];
    }

    //create a function that will allow you to call API endpoints at-will.
    protected function loadEndpoint($url, $method="GET", $data = "") {
        $this->printToConsole("$method $url");
        $body = json_encode($data);
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);                                                                             
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSLVERSION, 4);
        curl_setopt($ch, CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($body))
        );  
        $output = curl_exec($ch);
        if($output === false){
            $this->printToConsole(curl_error($ch));
            throw new Exception(curl_error($ch));
        }
        $info = curl_getinfo($ch);
        curl_close($ch);
        return array(
          'body' => $output,
          'info' => $info
        );
    }
    
    // wrappers for common HTTP verbs
    protected function get($url){
        return $this->loadEndpoint($url, "GET");
    }
    protected function post($url, $data){
        return $this->loadEndpoint($url, "POST", $data);
    }
    protected function put($url, $data){
        return $this->loadEndpoint($url, "PUT", $data);
    }
    protected function patch($url, $data){
        return $this->loadEndpoint($url, "PATCH", $data);
    }
    protected function delete($url){
        return $this->loadEndpoint($url, "DELETE");
    }

    // this allows you to write messages in the test output
    protected function printToConsole($statement) {
        fwrite(STDOUT, $statement."\n");
    }

    // this is just to avoid triggering warnings on PHPUnit
    public function testMock(){
        return true;
    }

}