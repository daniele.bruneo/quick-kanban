<?php

namespace Tests\Functional;

class DemoTest extends BaseTestCase
{
    /**
     * Test demo data contains name and count 9 elements
     */
    public function testGetDemo()
    {
        $response = $this->runApp('GET', '/demo');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains('name', (string)$response->getBody());
        $data = json_decode($response->getBody());
        $this->assertEquals(9, count($data));
    }

}