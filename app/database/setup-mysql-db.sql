CREATE DATABASE quick_kanban;

CREATE USER 'quickkanban'@'localhost';

GRANT ALL PRIVILEGES ON quick_kanban.* To 'quickkanban'@'localhost' IDENTIFIED BY 'getitdone';

FLUSH PRIVILEGES;