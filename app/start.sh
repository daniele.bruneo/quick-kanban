#!/bin/bash
trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

echo "Lanuching app..."
(
 cd backend/
 echo "Backend Starting..."
 composer start
) &
(
 cd frontend/
 echo "Frontend Starting..."
 npm start
) &

echo "Running..."
echo "CTRL+C to exit both servers"
while :
do
 sleep 1
done
echo "Bye"
