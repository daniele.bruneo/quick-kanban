# QuickKanban
A nice Kanban webapp to quickly create shareable todos boards.

![](res/screen-02.png)

Try it live on:

https://kanban.viralds.it

## Features
* Public Kanban boards
* Easy Drag&Drop
* Edit in place of everything
* Custom colors and background

## Application structure
This application is basically made up of these components:
* Backend REST API
* Frontend AngularJS UI
* Database

### Backend
Backend for this web app is provided by a set of REST API. 
These are written in PHP7 with Slim Framework v3.1.

The official Slim skeleton has been used to bootstrap the project:
https://github.com/slimphp/Slim-Skeleton

Eloquent and phpmig support were added.

### Frontend
Frontend UI is provided by an AngularJS web app.
AngularJS version used is v1.5.11

The official AngularJS seed has been used to bootstrap the project:
https://github.com/angular/angular-seed

LESS is used as CSS preprocessor.
